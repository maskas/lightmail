<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/messages', 'Api\MessagesController@index');
Route::get('/message', 'Api\MessageController@index');
Route::patch('/message/read', 'Api\MessageReadController@index');
Route::patch('/message/archive', 'Api\MessageArchiveController@index');
