
## About Lightmail

Considerations

- not all mess, that's being created by default in Laravel has been cleaned up
- an assumption has been made, that there will be no very large messages and total count of them won't be large as well.
- authentication credentials are hardcoded as constants. That's definitely not a perfect way of doing.
- it would be useful to include total messages / pages count and current page in /api/messages response
- it is likely, that reading message should be restricted if the message is currently archived

## Usage

- checkout the code from https://maskas@bitbucket.org/maskas/lightmail.git
- composer install
- create an empty MySQL database and setup database config in "config/database.php" or .env
- php artisan migrate:install
- php artisan migrate
- php artisan -v lightmail:import-data messages_sample.json
- php artisan serve
- access API via http://localhost:8000/api/... Default username and password: xxx and yyy.
- API documentation in docs/index.html file


## Tests

To run tests, execute
 
- vendor/bin/phpunit
- vendor/bin/phpspec run

## License

The Lihtmail is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).


