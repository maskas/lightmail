<?php
namespace Lightmail\Domain;

class Message
{
    /**
     * @var int
     */
    private $uid;
    /**
     * @var string
     */
    private $sender;
    /**
     * @var string
     */
    private $subject;
    /**
     * @var string
     */
    private $message;
    /**
     * @var int
     */
    private $timeSent;
    /**
     * @var bool
     */
    private $isArchived;
    /**
     * @var bool
     */
    private $isRead;

    /**
     * Email constructor.
     * @param int $uid
     * @param string $sender
     * @param string $subject
     * @param string $message
     * @param int $timeSent
     */
    public function __construct(
        $uid,
        $sender,
        $subject,
        $message,
        $timeSent
    ) {
        $this->uid = $uid;
        $this->sender = $sender;
        $this->subject = $subject;
        $this->message = $message;
        $this->timeSent = $timeSent;
        $this->isArchived = false;
        $this->isRead = false;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getTimeSent()
    {
        return $this->timeSent;
    }

    /**
     * @return bool
     */
    public function isArchived()
    {
        return $this->isArchived;
    }

    /**
     * @return bool
     */
    public function isRead()
    {
        return $this->isRead;
    }

    public function archive()
    {
        $this->isArchived = true;
    }

    public function read()
    {
        $this->isRead = true;
    }
}
