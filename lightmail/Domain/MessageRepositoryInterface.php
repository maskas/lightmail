<?php
namespace Lightmail\Domain;

use Lightmail\Domain\Exception\EntityNotFound;

interface MessageRepositoryInterface
{

    /**
     * Get message by uid or fail
     * @param string $uid
     * @return Message
     * @throws EntityNotFound
     */
    public function get($uid);

    /**
     * @param int $page
     * @param int $pageSize
     * @param bool $archivedOnly
     * @return Message[]
     */
    public function findPaginated($page, $pageSize, $archivedOnly);

    /**
     * @param Message $email
     */
    public function save(Message $email);
}