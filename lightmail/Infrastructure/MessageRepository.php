<?php

namespace Lightmail\Infrastructure;

use Doctrine\ORM\EntityRepository;
use Lightmail\Domain\Exception\EntityNotFound;
use Lightmail\Domain\Message;
use Lightmail\Domain\MessageRepositoryInterface;

class MessageRepository extends EntityRepository implements MessageRepositoryInterface
{

    /**
     * Get message by uid or fail
     * @param string $uid
     * @return Message
     * @throws EntityNotFound
     */
    public function get($uid)
    {
        $message = $this->find($uid);
        if (!$message) {
            throw new EntityNotFound("Entity not found. UID: " . $uid);
        }
        return $message;
    }

    /**
     * @param int $page page index (starts at 0)
     * @param int $pageSize messages per page. Null means unlimited
     * @param bool $archivedOnly exclude all non archived messages
     * @return Message[]
     */
    public function findPaginated($page, $pageSize, $archivedOnly)
    {
        $filter = [];
        if ($archivedOnly) {
            $filter = ['isArchived' => 1];
        }
        return $this->findBy(
            $filter,
            ['timeSent' => 'desc'],
            $pageSize,
            $pageSize * $page
        );
    }

    /**
     * @param Message $message
     */
    public function save(Message $message)
    {
        $this->_em->persist($message);
        $this->_em->flush($message);
    }
}