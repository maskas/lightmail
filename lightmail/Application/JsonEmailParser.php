<?php


namespace Lightmail\Application;


use JsonSchema\Validator;
use Lightmail\Application\Exception\ArgumentException;
use Lightmail\Domain\Message;

class JsonEmailParser
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * JsonEmailParser constructor.
     * @param Validator $validator
     */
    public function __construct(
        Validator $validator
    ) {
        $this->validator = $validator;
    }
    /**
     * @param $json
     * @return Message[]
     * @throws ArgumentException
     */
    public function parse($json) {
        $data = json_decode($json);

        $this->validator->reset();
        $this->validator->check($data, $this->messagesJsonSchema());
        if (!$this->validator->isValid()) {
            $message = "SON does not validate. Violations:\n";
            foreach ($this->validator->getErrors() as $error) {
                $message .= sprintf("[%s] %s\n", $error['property'], $error['message']);
            }
            throw new ArgumentException($message);
        }

        $messages = [];
        foreach ($data->messages as $key => $message) {
            $messages[] = $this->buildEmail($message);
        }

        return $messages;
    }

    /**
     * @param Object $emailData
     * @return Message
     * @throws ArgumentException
     */
    private function buildEmail($emailData) {
        $email = new Message(
            (string) $emailData->uid,
            (string) $emailData->sender,
            (string) $emailData->subject,
            (string) $emailData->message,
            (int) $emailData->time_sent
        );
        return $email;
    }

    private function messagesJsonSchema()
    {
        return (object)[
            "\$schema" => "http://json-schema.org/draft-04/schema#",
            "title" => "Emails",
            "description" => "Emails data to be imported",
            "type" => "object",
            "properties" => [
                "messages" => (object)[
                    "description" => "An array of email messages",
                    "type" => "array",
                    "items" => [
                        "type" => "object",
                        "properties" => [
                            "uid" => (object)[
                                "description" => "Unique ID of the messages",
                                "type" => "string"
                            ],
                            "sender" => (object)[
                                "description" => "Full name of the sender",
                                "type" => "string"
                            ],
                            "subject" => (object)[
                                "description" => "Email subject",
                                "type" => "string"
                            ],
                            "message" => (object)[
                                "description" => "Email message",
                                "type" => "string"
                            ],
                            "time_sent" => (object)[
                                "description" => "Unix timestamp",
                                "type" => "integer"
                            ]
                        ],
                        "required" => ["uid", "sender", "subject", "message", "time_sent"]
                    ]
                ]
            ],
            "required" => ["messages"]
        ];
    }
}