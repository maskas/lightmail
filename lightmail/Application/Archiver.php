<?php


namespace Lightmail\Application;


use Lightmail\Domain\Exception\EntityNotFound;
use Lightmail\Domain\MessageRepositoryInterface;
use Lightmail\Infrastructure\MessageRepository;

class Archiver
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * Archiver constructor.
     * @param MessageRepositoryInterface $messageRepository
     */
    public function __construct(
        MessageRepositoryInterface $messageRepository
    )
    {

        $this->messageRepository = $messageRepository;
    }

    /**
     * @param string $uid
     * @throws EntityNotFound
     */
    public function archive($uid)
    {
        $message = $this->messageRepository->get($uid);
        $message->archive();
        $this->messageRepository->save($message);
    }
}