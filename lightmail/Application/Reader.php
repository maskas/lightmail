<?php


namespace Lightmail\Application;


use Lightmail\Domain\Exception\EntityNotFound;
use Lightmail\Domain\MessageRepositoryInterface;
use Lightmail\Infrastructure\MessageRepository;

class Reader
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * Archiver constructor.
     * @param MessageRepositoryInterface $messageRepository
     */
    public function __construct(
        MessageRepositoryInterface $messageRepository
    )
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @param string $uid
     * @throws EntityNotFound
     */
    public function read($uid)
    {
        $message = $this->messageRepository->get($uid);
        $message->read();
        $this->messageRepository->save($message);
    }
}