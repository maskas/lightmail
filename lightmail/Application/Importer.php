<?php
namespace Lightmail\Application;


use Lightmail\Domain\MessageRepositoryInterface;

class Importer
{
    /**
     * @var JsonEmailParser
     */
    private $jsonEmailParser;
    /**
     * @var MessageRepositoryInterface
     */
    private $messageRepository;

    /**
     * Importer constructor.
     * @param MessageRepositoryInterface $messageRepository
     * @param JsonEmailParser $jsonEmailParser
     */
    public function __construct(
        MessageRepositoryInterface $messageRepository,
        JsonEmailParser $jsonEmailParser
    ) {
        $this->jsonEmailParser = $jsonEmailParser;
        $this->messageRepository = $messageRepository;
    }

    /**
     * @param $json
     */
    function import($json) {
        $emails = $this->jsonEmailParser->parse($json);
        foreach ($emails as $email) {
            $this->messageRepository->save($email);
        }
    }
}