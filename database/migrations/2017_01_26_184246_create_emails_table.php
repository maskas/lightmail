<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->string('id', 255)->primary();
            $table->string('sender', 255);
            $table->text('subject', 255);
            $table->longText('message');
            $table->bigInteger('time_sent');
            $table->boolean('is_archived')->default(false)->index();
            $table->boolean('is_read')->default(false)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
