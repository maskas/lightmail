<?php


namespace App\Providers;


use Doctrine\ORM\EntityManager;
use Illuminate\Support\ServiceProvider;
use Lightmail\Domain\Message;
use Lightmail\Domain\MessageRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind(MessageRepositoryInterface::class, function($app)
        {
            /** @var EntityManager $entityManager */
            $entityManager = $app['em'];
            return $entityManager->getRepository(Message::class);
        });
    }

}