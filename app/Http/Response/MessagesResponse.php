<?php
namespace App\Http\Response;

use Illuminate\Http\JsonResponse;
use Lightmail\Domain\Message;

class MessagesResponse extends JsonResponse
{

    /**
     * @param Message[] $messages
     * @return MessagesResponse
     */
    public function __construct($messages) {
        $data = $this->formatData($messages);
        parent::__construct($data);
    }

    /**
     * @param Message[] $messages
     * @return array
     */
    private function formatData($messages) {
        $data = [];
        $data['messages'] = [];
        foreach ($messages as $key => $message) {
            $messageData = [
                'uid' => $message->getUid(),
                'sender' => $message->getSender(),
                'subject' => $message->getSubject(),
                'message' => $message->getMessage(),
                'time_sent' => $message->getTimeSent(),
                'is_read' => $message->isRead(),
                'is_archived' => $message->isArchived(),
            ];
            $data['messages'][] = $messageData;
        }
        return $data;
    }
}
