<?php
namespace App\Http\Response;

use Illuminate\Http\JsonResponse;
use Lightmail\Domain\Message;

class SuccessResponse extends JsonResponse
{

    /**
     * @param Message $message
     * @return MessageResponse
     */
    public function __construct($message) {
        $data = [
            'message' => $message
        ];
        parent::__construct($data);
    }
}
