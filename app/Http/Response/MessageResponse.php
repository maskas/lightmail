<?php
namespace App\Http\Response;

use Illuminate\Http\JsonResponse;
use Lightmail\Domain\Message;

class MessageResponse extends JsonResponse
{

    /**
     * @param Message $message
     * @return MessageResponse
     */
    public function __construct($message) {
        $data = $this->formatData($message);
        parent::__construct($data);
    }

    /**
     * @param $message
     * @return array
     */
    private function formatData($message) {
        $data = [
            'uid' => $message->getUid(),
            'sender' => $message->getSender(),
            'subject' => $message->getSubject(),
            'message' => $message->getMessage(),
            'time_sent' => $message->getTimeSent(),
            'is_read' => $message->isRead(),
            'is_archived' => $message->isArchived(),
        ];
        return $data;
    }
}
