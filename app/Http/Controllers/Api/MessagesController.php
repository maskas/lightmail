<?php


namespace App\Http\Controllers\Api;

use App\Exceptions\ArgumentException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Response\MessagesResponse;
use Illuminate\Http\Request;
use Lightmail\Domain\MessageRepositoryInterface;

class MessagesController extends Controller
{
    /**
     * @var MessageRepositoryInterface
     */
    private $messageRepository;
    /**
     * @var Request
     */
    private $request;

    /**
     * MessagesController constructor.
     * @param MessageRepositoryInterface $messageRepository
     * @param Request $request
     */
    public function __construct(
        MessageRepositoryInterface $messageRepository,
        Request $request
    )
    {

        $this->messageRepository = $messageRepository;
        $this->request = $request;
    }

    /**
     * @return MessagesResponse
     * @throws ArgumentException
     */
    public function index()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'page' => 'integer|min:0',
                'pageSize' => 'integer|min:1',
                'archivedOnly' => 'boolean',
            ]
        );
        if ($validator->fails()) {
            throw new ArgumentException("Invalid arguments");
        }
        $messagesResponse = new MessagesResponse(
            $this->messageRepository->findPaginated(
                $this->request->get('page', 0),
                $this->request->get('pageSize', 10),
                $this->request->get('archivedOnly', 0)
            )
        );

        return $messagesResponse;
    }
}