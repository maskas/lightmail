<?php


namespace App\Http\Controllers\Api;

use App\Exceptions\ArgumentException;
use App\Http\Response\MessageResponse;
use App\Http\Response\SuccessResponse;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lightmail\Application\Archiver;

class MessageArchiveController extends Controller
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Archiver
     */
    private $archiver;

    /**
     * MessagesController constructor.
     * @param Request $request
     * @param Archiver $archiver
     */
    public function __construct(
        Request $request,
        Archiver $archiver
    )
    {
        $this->request = $request;
        $this->archiver = $archiver;
    }

    /**
     * @return MessageResponse
     * @throws ArgumentException
     */
    public function index()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'uid' => 'required|string'
            ]
        );
        if ($validator->fails()) {
            throw new ArgumentException("Invalid arguments");
        }


        $this->archiver->archive($this->request->get('uid'));
        return new SuccessResponse("Message has been archived.");
    }
}
