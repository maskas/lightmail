<?php


namespace App\Http\Controllers\Api;

use App\Exceptions\ArgumentException;
use App\Http\Response\MessageResponse;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lightmail\Domain\MessageRepositoryInterface;

class MessageController extends Controller
{
    /**
     * @var MessageRepositoryInterface
     */
    private $messageRepository;
    /**
     * @var Request
     */
    private $request;

    /**
     * MessagesController constructor.
     * @param MessageRepositoryInterface $messageRepository
     * @param Request $request
     */
    public function __construct(
        MessageRepositoryInterface $messageRepository,
        Request $request
    )
    {

        $this->messageRepository = $messageRepository;
        $this->request = $request;
    }

    /**
     * @return MessageResponse
     * @throws ArgumentException
     */
    public function index()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'uid' => 'required|string'
            ]
        );
        if ($validator->fails()) {
            throw new ArgumentException("Invalid arguments");
        }

        $message = $this->messageRepository->get($this->request->get('uid'));
        $messagesResponse = new MessageResponse($message);

        return $messagesResponse;
    }
}