<?php


namespace App\Http\Controllers\Api;

use App\Exceptions\ArgumentException;
use App\Http\Response\MessageResponse;
use App\Http\Response\SuccessResponse;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lightmail\Application\Reader;

class MessageReadController extends Controller
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Reader
     */
    private $reader;

    /**
     * MessagesController constructor.
     * @param Request $request
     * @param Reader $reader
     */
    public function __construct(
        Request $request,
        Reader $reader
    )
    {
        $this->request = $request;
        $this->reader = $reader;
    }

    /**
     * @return MessageResponse
     * @throws ArgumentException
     */
    public function index()
    {
        $validator = Validator::make(
            $this->request->all(),
            [
                'uid' => 'required|string'
            ]
        );
        if ($validator->fails()) {
            throw new ArgumentException("Invalid arguments");
        }

        $this->reader->read($this->request->get('uid'));
        return new SuccessResponse("Message has been read.");
    }
}
