<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class EasyAuth
{
    const HTTP_AUTH_USER = 'xxx';
    const HTTP_AUTH_PW = 'yyy';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->authenticationNeeded()) {
            $headers = [
                'WWW-Authenticate' => 'Basic realm="My Realm"'
            ];

            return new Response('Access denied.', Response::HTTP_UNAUTHORIZED, $headers);
        }

        return $next($request);
    }

    private function authenticationNeeded()
    {
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
            return true;
        }
        if ($_SERVER['PHP_AUTH_USER'] !== self::HTTP_AUTH_USER || $_SERVER['PHP_AUTH_PW'] !== self::HTTP_AUTH_PW) {
            return true;
        }
        
        return false;
    }
}
