<?php

namespace App\Console\Commands;

use App\Exceptions\ArgumentException;
use Illuminate\Console\Command;
use Lightmail\Application\Importer;

class ImportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lightmail:import-data {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from a JSON file';
    /**
     * @var Importer
     */
    private $importer;

    /**
     * ImportData constructor.
     * @param Importer $importer
     */
    public function __construct(
        Importer $importer
    ) {
        parent::__construct();
        $this->importer = $importer;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $file = $this->argument('file');
        if (!is_file($file)) {
            throw new ArgumentException("File does not exist " . $file);
        }
        $this->importer->import(file_get_contents($file));
    }
}
