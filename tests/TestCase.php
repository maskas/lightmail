<?php

use App\Http\Middleware\EasyAuth;
use Lightmail\Domain\MessageRepositoryInterface;
use Mock\AuthenticationMock;
use Mock\MessageRepositoryMock;

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost:8000';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        $this->mockRepository($app);

        $this->mockAuthentication($app);

        return $app;
    }

    public function mockRepository($app)
    {
        $app->bind(MessageRepositoryInterface::class, function($app)
        {
            return new MessageRepositoryMock();
        });
    }

    public function mockAuthentication($app)
    {
        $app->bind(EasyAuth::class, function($app)
        {
            return new AuthenticationMock();
        });
    }
}
