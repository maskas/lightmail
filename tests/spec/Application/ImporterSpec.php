<?php

namespace spec\Lightmail\Application;


use Lightmail\Application\JsonEmailParser;
use Lightmail\Domain\Message;
use Lightmail\Domain\MessageRepositoryInterface;
use PhpSpec\ObjectBehavior;

class ImporterSpec extends ObjectBehavior
{
    function let(
        MessageRepositoryInterface $messageRepository,
        JsonEmailParser $jsonEmailParser
    ) {
        $this->beConstructedWith($messageRepository, $jsonEmailParser);
    }

    function it_stores_data_from_json (
        MessageRepositoryInterface $messageRepository,
        JsonEmailParser $jsonEmailParser,
        Message $email1,
        Message $email2
    ) {

        $jsonEmailParser->parse('')->shouldBeCalled()->willReturn([$email1, $email2]);

        $messageRepository->save($email1)->shouldBeCalled();
        $messageRepository->save($email2)->shouldBeCalled();

        $this->import('');
    }

    function it_doesnt_fail_on_empty_data (
        JsonEmailParser $jsonEmailParser
    ) {
        $jsonEmailParser->parse('')->shouldBeCalled()->willReturn([]);
        $this->import('');
    }
}