<?php

namespace spec\Lightmail\Application;


use JsonSchema\Validator;
use Lightmail\Domain\Message;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class JsonEmailParserSpec extends ObjectBehavior
{
    function let(
        Validator $validator
    ) {
        $this->beConstructedWith($validator);
    }

    function it_parses_json_into_email_objects (
        Validator $validator
    ) {
        $sampleData = [
            "messages" => [
                [
                    "uid" => "21",
                    "sender" => "Ernest Hemingway",
                    "subject" => "animals",
                    "message" => "This is a tale about nihilism.",
                    "time_sent" => 1459239867
                ],
                [
                    "uid" => "22",
                    "sender" => "Stephen King",
                    "subject" => "adoration",
                    "message" => "The story is about a fire fighter.",
                    "time_sent" => 1459248747
                ]
            ]
        ];
        $jsonSampleData = json_encode($sampleData);

        $validator->reset()->shouldBeCalled();
        $validator->isValid()->willReturn(true);
        $validator->check(Argument::any(), Argument::any())->shouldBeCalled();

        /**
         * @var Message[] $result
         */
        $result = $this->parse($jsonSampleData);
        $result->shouldHaveCount(2);
        $result[0]->getUid()->shouldEqual("21");
        $result[0]->getSender()->shouldEqual("Ernest Hemingway");
        $result[0]->getSubject()->shouldEqual("animals");
        $result[0]->getMessage()->shouldEqual("This is a tale about nihilism.");
        $result[0]->getTimeSent()->shouldEqual(1459239867);
        $result[0]->isRead()->shouldEqual(false);
        $result[0]->isArchived()->shouldEqual(false);
    }
}