<?php


namespace Mock;

use Closure;
use App\Http\Middleware\EasyAuth;

class AuthenticationMock extends EasyAuth
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}