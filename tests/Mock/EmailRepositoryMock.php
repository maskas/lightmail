<?php
namespace Mock;

use Lightmail\Domain\Exception\EntityNotFound;
use Lightmail\Domain\Message;

class MessageRepositoryMock implements \Lightmail\Domain\MessageRepositoryInterface
{
    private static $messages = [];

    public static function mockMessages($messages)
    {
        self::$messages = $messages;
    }

    /**
     * @param \Lightmail\Domain\Message $email
     */
    public function save(\Lightmail\Domain\Message $email)
    {

    }

    /**
     * Get message by uid or fail
     * @param string $uid
     * @return Message
     * @throws EntityNotFound
     */
    public function get($uid)
    {

    }

    /**
     * @param int $page
     * @param int $pageSize
     * @param bool $archivedOnly
     * @return Message[]
     */
    public function findPaginated($page, $pageSize, $archivedOnly)
    {
        //we ignore pagination in the mock for simplicity
        return self::$messages;
    }
}