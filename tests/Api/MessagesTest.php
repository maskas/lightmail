<?php


use Lightmail\Domain\Message;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $messages = [];
        $messages[] = new Message(
            21,
            "Ernest Hemingway",
            "animals",
            "This is a tale about nihilism.",
            1459239867
        );
        $messages[] = new Message(
            22,
            "Stephen King",
            "adoration",
            "The story is about a fire fighter.",
            1459248747
        );

        \Mock\MessageRepositoryMock::mockMessages($messages);

        $this->visit('/api/messages')
            ->seeJson([
                "messages" => [
                    [
                        "is_archived" => false,
                        "is_read" => false,
                        "uid" => 21,
                        "sender" => "Ernest Hemingway",
                        "subject" => "animals",
                        "message" => "This is a tale about nihilism.",
                        "time_sent" => 1459239867
                    ],
                    [
                        "is_archived" => false,
                        "is_read" => false,
                        "uid" => 22,
                        "sender" => "Stephen King",
                        "subject" => "adoration",
                        "message" => "The story is about a fire fighter.",
                        "time_sent" => 1459248747
                    ]
                ]
            ]);
    }
}
